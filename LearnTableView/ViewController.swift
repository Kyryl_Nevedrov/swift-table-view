//
//  ViewController.swift
//  LearnTableView
//
//  Created by Admin on 28.06.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var showText: UILabel!
    @IBOutlet weak var myTableView: UITableView!
    
   
    @IBAction func deleteSelected(sender: UIButton) {
       
        let indexPathSelectedArray = myTableView.indexPathsForSelectedRows?.sort({ firstIndexPath, secondIndexPath in
            return   firstIndexPath.row > secondIndexPath.row && firstIndexPath.section == secondIndexPath.section
        })
        
        for index in indexPathSelectedArray ?? [] {
            productLines[index.section].products.removeAtIndex(index.row)
        }
        
        myTableView.reloadData()
    }
  
    @IBAction func DeleteAll(sender: UIButton) {
        for j in 0..<myTableView.numberOfSections {
            for i in (myTableView.numberOfRowsInSection(j) - 1).stride(through: 0, by: -1) {
                productLines[j].products.removeAtIndex(i)
                myTableView.reloadData()
            }
        }
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myTableView.allowsMultipleSelection = true
    }
    
    lazy var productLines: [ProductLine] = {
        return ProductLine.productLines()
    }()
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        let selctedRow = tableView.cellForRowAtIndexPath(indexPath)!
        
        if editingStyle == UITableViewCellEditingStyle.Delete {
            productLines[indexPath.section].products.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            selctedRow.accessoryType = UITableViewCellAccessoryType.None
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let delete = UITableViewRowAction(style: .Destructive, title: "Delete") { (action, indexPath) in
            self.tableView( tableView, commitEditingStyle: UITableViewCellEditingStyle.Delete, forRowAtIndexPath: indexPath)
        }
        
        let show = UITableViewRowAction(style: .Destructive, title: "Show") { (action, indexPath) in
            self.showText.text = self.productLines[indexPath.section].products[indexPath.row].title
        }
        
        
        delete.backgroundColor = UIColor.blackColor()
        show.backgroundColor = UIColor.blueColor()
        
        return [delete, show]
    }
    
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    // MARK: - Table view data source
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let productLine = productLines[section]
        return productLine.name
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return productLines.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let productLine = productLines[section]
        return productLine.products.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        let productLine = productLines[indexPath.section]
        let product = productLine.products[indexPath.row]
        
        // Configure the cell...
        cell.textLabel?.text = product.title
        cell.detailTextLabel?.text = product.description
        cell.imageView?.image = product.image
     
        return cell
    }

}

